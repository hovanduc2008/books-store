<div class="slider">
    <div class="container">
        <div class="left">
            <div class="contain-img">
                <img src="https://cdn0.fahasa.com/media/magentothem/banner7/AnSinh_840x320.png" alt="">
                <img src="https://cdn0.fahasa.com/media/magentothem/banner7/Fahasa_saleT3_Tuan2_mainbanner_banner_840x320.jpg" alt="">
                <img src="https://cdn0.fahasa.com/media/magentothem/banner7/Manga_mainbanner_T11_Slide_840x320.jpg" alt="">
                <img class = "active" src="https://cdn0.fahasa.com/media/magentothem/banner7/NCCThienLongT1123_BannerSlide_840x320.jpg" alt="">
            </div>
            <div class="slider-control">
                <div class="prev"><i class="fa-solid fa-chevron-left"></i></div>
                <div class="next"><i class="fa-solid fa-chevron-right"></i></div>
            </div>
            <div class="pagination">
                <div class="pgn-list">
                    <p class = "pgn-active"></p>
                    <p></p>
                    <p></p>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="right">
            <div class="contain-img">
                <img src="https://cdn0.fahasa.com/media/wysiwyg/Thang-11-2023/PoticoT11_392x165.png" alt="">
            </div>
            <div class="contain-img">
                <img src="https://cdn0.fahasa.com/media/wysiwyg/Thang-11-2023/ZaloPay11_392x156_1.jpg" alt="">
            </div>
        </div>
    </div>
</div>