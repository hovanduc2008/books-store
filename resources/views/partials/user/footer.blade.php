<footer class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="intro">
                @include('partials.user.logo')
                <p class="content">
                    Books Store là một trang web đa chức năng nhằm cung cấp cho người đọc một trải nghiệm tuyệt vời trong việc mua sắm và sử dụng sách. Mục đích chính của chúng tôi là tạo ra một nền tảng thuận tiện và đa dạng, nơi mọi người có thể dễ dàng tìm kiếm, mua và thậm chí cho mượn những tác phẩm văn hóa giáo dục.
                </p>
            </div>
            <div class="lecturer">
                <h4><span>GIẢNG VIÊN</span></h4>
                <p class = "content1">Giảng viên hướng dẫn: <br>Ths.Phạm Thế Anh</p>
                <p class = "content2">
                    Giảng viên hướng dẫn tại trường là một người nắm vững kiến thức chuyên ngành và đặc biệt là một nguồn động viên lớn cho sinh viên. Với sự hiểu biết sâu sắc về lĩnh vực mình giảng dạy, giảng viên không chỉ là người truyền đạt kiến thức mà còn là người tạo nên môi trường học tập tích cực và sáng tạo.
                </p>
            </div>
            <div class="members">
                <h4><span>THÀNH VIÊN</span></h4>
                <p class = "content1">Với sự tham dự, đóng góp của các thành viên: </p>
                <ul style = "font-size: 1.3rem">
                    <li>Sinh viên: Hồ Văn Đức - 2021608034</li>
                    <li>Sinh viên: Nguyễn Văn Nguyên - 2020605636</li>
                    <li>Sinh viên: Phạm Minh Đức - 2020606201</li>
                    <li>Sinh viên: Trần Thành Vinh - 2020605175</li>
                    <li>Sinh viên: Đào Công Đoàn - 2020606491</li>
                </ul>
                <p class = "content2">Trong quá trình xây dựng website, thành viên đóng một vai trò quan trọng, đóng góp đa dạng kỹ năng và tâm huyết để tạo ra một trang web chất lượng và thân thiện với người dùng.</p>
            </div>
            <div class="document">
                <h4><span>TÀI LIỆU</span></h4>
                <p>Giáo trình PHP</p>
            </div>
        </div>
        <div class="footer-bottom">
            <span>Copyright &copy; Group 9 - PHP2_S2_T9_2023</span>
        </div>
    </div>
</footer>